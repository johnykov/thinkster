## MEAN thinkster
0. boot2docker up
   1. docker start meanMong
   2. docker start bootzookaMongo
2.  docker ps

3. docker pull mongo:latest
4. docker run --name somename -d -p 27017:27017 mongo


### start serving

1. cd flapper-news
2. npm start 
   (fires bin/www)

### test mongo

curl --data 'title=test&link=http://test.com' http://localhost:3000/posts
curl http://localhost:3000/posts

[tutorial source](https://thinkster.io/angulartutorial/mean-stack-tutorial/)
